﻿namespace ExpressionParser
{
    public enum CharacterType
    {
        Invalid,
        Digit,
        Plus,
        Minus,
        Multiply,
        Divide,
        OpenBracket,
        CloseBracket,
        None
    }
}
