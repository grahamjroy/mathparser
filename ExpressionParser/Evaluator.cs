﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ExpressionParser
{
    public class Evaluator
    {
        //3ae4c66fb32
        //3+
        // ( 4*
        //   66 ) -
        //32
        private List<EvaluatorOperation> _operations;
        
        public Evaluator()
        {
            _operations = new List<EvaluatorOperation>();
        }

        public int OpenBrackets { get; private set; }
        public int ClosedBrackets { get; private set; }
        public bool IsOpen { get; set; }

        public void Append(ParsedCharacter character)
        {
            switch (character.Type)
            {
                case CharacterType.Plus:
                case CharacterType.Minus:
                case CharacterType.Multiply:
                case CharacterType.Divide:
                    AddOperator(character);
                    break;
                case CharacterType.Digit:
                    AddDigit(character);
                    break;
                case CharacterType.OpenBracket:
                    StartBrackets(character);
                    break;
                case CharacterType.CloseBracket:
                    CloseBrackets(character);
                    break;
            }
        }

        public Result Evaluate()
        {
            var currentCharacterType = CharacterType.None;
            decimal currentValue = 0;
            foreach (var operation in _operations)
            {
                decimal value = 0;
                if (operation.Operation != null)
                {
                    var subResult = operation.Operation.Evaluate();
                    if (!subResult.WasValidInput)
                        return subResult;

                    value = subResult.Output;
                }
                else
                {
                    value = operation.Value;
                }

                if (currentCharacterType != CharacterType.None)
                {

                    try
                    {
                        currentValue = calculate(currentValue, currentCharacterType, value);
                        currentCharacterType = operation.Operator;
                    }
                    catch (Exception ex)
                    {
                        return new Result(ex.Message);
                    }
                }
                else
                {
                    currentValue = value;
                    currentCharacterType = operation.Operator;
                }
            }

            return new Result(currentValue);
        }

        private decimal calculate(decimal currentValue, CharacterType operatorType, decimal operationValue)
        {
            switch (operatorType)
            {
                case CharacterType.Plus:
                    return currentValue + operationValue;
                case CharacterType.Minus:
                    return currentValue - operationValue;
                case CharacterType.Multiply:
                    return currentValue * operationValue;
                case CharacterType.Divide:
                    return currentValue / operationValue;
            }
            throw new Exception("Invalid character order, cannot evaluate equation");
        }

        private void AddDigit(ParsedCharacter character)
        {
            if (_operations.Count == 0)
            {
                _operations.Add(new EvaluatorOperation());
            }
            var operation = _operations[_operations.Count - 1];
            if (operation.Operator == CharacterType.None || (operation.Operation != null && operation.Operation.IsOpen))
            {
                operation.AddDigit(character);
            }
            else
            {
                _operations.Add(new EvaluatorOperation());
                _operations[_operations.Count - 1].AddDigit(character);
            }
        }

        private void AddOperator(ParsedCharacter character)
        {
            if (_operations.Count == 0)
            {
                throw new Exception("Invalid character");
            }
            _operations[_operations.Count - 1].AddOperator(character);
            //_operations.Add(new EvaluatorOperation());
        }

        private void StartBrackets(ParsedCharacter character)
        {
            OpenBrackets++;
            if (_operations.Count == 0)
            {
                _operations.Add(new EvaluatorOperation());
            }
            var operation = _operations[_operations.Count - 1];
            if (operation.Operator == CharacterType.None || (operation.Operation != null && operation.Operation.IsOpen))
            {
                operation.StartSubOperation(character);
            }
            else
            {
                _operations.Add(new EvaluatorOperation());
                _operations[_operations.Count - 1].StartSubOperation(character);
            }
        }

        public void CloseBrackets(ParsedCharacter character)
        {
            ClosedBrackets++;
            if (_operations.Count == 0)
            {
                throw new Exception("Invalid characters");
            }
            if (_operations[_operations.Count - 1].Operation != null)
            {
                //_operations.Add(new EvaluatorOperation());
                //IsOpen = false;
                _operations[_operations.Count - 1].CloseBrackets(character);
            }
            else
            {
                IsOpen = false;
            }
        }
    }
}
