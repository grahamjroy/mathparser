﻿namespace ExpressionParser
{
    public class CharacterProcessor : ICharacterProcessor
    {
        public ParsedCharacter ParseCharacter(char character)
        {
            switch (character)
            {
                case 'a':
                    return new ParsedCharacter(CharacterType.Plus);
                case 'b':
                    return new ParsedCharacter(CharacterType.Minus);
                case 'c':
                    return new ParsedCharacter(CharacterType.Multiply);
                case 'd':
                    return new ParsedCharacter(CharacterType.Divide);
                case 'e':
                    return new ParsedCharacter(CharacterType.OpenBracket);
                case 'f':
                    return new ParsedCharacter(CharacterType.CloseBracket);
                default:
                    int digitValue = 0;
                    if (int.TryParse(character.ToString(), out digitValue))
                    {
                        return new ParsedCharacter(digitValue);
                    }
                    return new ParsedCharacter(CharacterType.Invalid);
            }
        }
    }

}
