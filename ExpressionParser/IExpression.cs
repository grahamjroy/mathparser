﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    public interface IExpression
    {
        ParseResult Parse(string inputString);

        Result Evaluate();
    }
}
