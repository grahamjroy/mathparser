﻿namespace ExpressionParser
{
    public class Result
    {
        public Result(string errorMessage)
        {
            WasValidInput = false;
            ErrorMessage = errorMessage;
        }

        public Result(decimal output)
        {
            WasValidInput = true;
            Output = output;
        }

        public decimal Output { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool WasValidInput { get; private set; }
    }
}
