﻿namespace ExpressionParser
{
    public interface ICharacterProcessor
    {
        ParsedCharacter ParseCharacter(char character);
    }
}
