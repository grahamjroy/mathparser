﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    public class EvaluatorOperation
    {
        public decimal Value { get; private set; }
        public CharacterType Operator { get; private set; }
        public Evaluator Operation { get; private set; }

        public EvaluatorOperation()
        {
            Operator = CharacterType.None;
        }

        public void AddDigit(ParsedCharacter character)
        {
            if (Operation != null)
            {
                Operation.Append(character);
            }
            else
            {
                if (Value > 0)
                {
                    Value = Value * 10;
                }
                Value = Value + character.DigitValue;
            }
        }

        public void AddOperator(ParsedCharacter character)
        {
            if (Operation != null && Operation.IsOpen)
            {
                Operation.Append(character);
            }
            Operator = character.Type;
        }

        public void StartSubOperation(ParsedCharacter character)
        {
            if (Operation != null)
            {
                Operation.Append(character);
            }
            else
            {
                Operation = new Evaluator();
                Operation.IsOpen = true;
            }
        }

        public void CloseBrackets(ParsedCharacter character)
        {
            if (Operation == null)
            {
                throw new Exception("Cannot close brackets before opening them, invalid expression string.");
            }
            Operation.Append(character);
            //Operation.CloseBrackets();
            //Operation.IsOpen = false;
        }
    }
}
