﻿using System;
using System.Linq;

namespace ExpressionParser
{
    public class Expression : IExpression
    {
        private readonly ICharacterProcessor _characterProcessor;
        private Evaluator _evaluator;

        public Expression()
        {
            _characterProcessor = new CharacterProcessor();
        }

        public Expression(ICharacterProcessor characterProcessor)
        {
            _characterProcessor = characterProcessor;
        }

        public ParseResult Parse(string inputString)
        {
            _evaluator = new Evaluator();
            var characters = inputString.ToCharArray();
            for (int i = 0; i < characters.Count(); i++)
            {
                var parsedCharacter = _characterProcessor.ParseCharacter(characters[i]);
                try
                {
                    _evaluator.Append(parsedCharacter);
                }
                catch (Exception ex)
                {
                    return new ParseResult(ex.Message);
                }
            }
            if (_evaluator.ClosedBrackets > _evaluator.OpenBrackets)
            {
                return new ParseResult("Too Many closing brackets.");
            }
            if (_evaluator.OpenBrackets > _evaluator.ClosedBrackets)
            {
                return new ParseResult("Not enough closing brackets");
            }
            return new ParseResult();
        }

        public Result Evaluate()
        {
            return _evaluator.Evaluate();
        }
    }
}
