﻿namespace ExpressionParser
{
    public class ParsedCharacter
    {
        public ParsedCharacter(CharacterType type)
        {
            Type = type;
        }

        public ParsedCharacter(int value)
        {
            Type = CharacterType.Digit;
            DigitValue = value;
        }

        public CharacterType Type { get; private set; }
        public int DigitValue { get; private set; }
    }
}
