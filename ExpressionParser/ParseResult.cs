﻿namespace ExpressionParser
{
    public class ParseResult
    {
        public ParseResult(string errorMessage)
        {
            WasValidInput = false;
            ErrorMessage = errorMessage;
        }

        public ParseResult()
        {
            WasValidInput = true;
        }

        public string ErrorMessage { get; private set; }
        public bool WasValidInput { get; private set; }
    }
}
