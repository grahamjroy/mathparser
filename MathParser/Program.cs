﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ExpressionParser;

namespace MathParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mathematical Parser");
            Console.WriteLine("Please enter your character string");
            Console.WriteLine("Type 'exit' to quit");

            var parser = new Expression();

            var input = "";
            //check the user doesn't want to exit
            while ((input = Console.ReadLine()) != "exit")
            {
                //validate the input string only contains the characters allowed
                if(ValidateString(input))
                {
                    var parseResult = parser.Parse(input);

                    if (parseResult.WasValidInput)
                    {
                        var result = parser.Evaluate();
                        if (result.WasValidInput)
                        {
                            Console.WriteLine("Successfully parsed, output = " + result.Output);
                        }
                        else
                        {
                            Console.WriteLine(result.ErrorMessage);
                        }
                    }
                    else
                    {
                        Console.WriteLine(parseResult.ErrorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// Takes the input string and returns true if all the error criteria are avoided
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static bool ValidateString(string s)
        {
            //checks only valid characters are entered
            return Regex.IsMatch(s, @"^[0-9a-f]*$");
            
            //TODO: check that string is in the correct format? i.e. check the brackets are corresponding, there are no adjacent operators
            //checks:
            //1. you can't have two letters next to each other
            //2a. you can't have an opening bracket before a closing one
            //2b. you must have a corresponding number of closing brakcets to opening ones
        }
    }
}
