﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MathParser
{
    [TestFixture]
    class TestClass
    {
        [Test]
        public void ImplementationTests()
        {
            // arrange
            var input1 = "3a2c4";
            var input2 = "32a2d2";
            var input3 = "500a10b66c32";
            var input4 = "3ae4c66fb32";
            var input5 = "3c4d2aee2a4c41fc4f";

            // act
            var testProg = new MathParser.Program(); 
            //TODO: is there a way of doing this woithout publicly exposing the internal methods?
            //And can this be done in a static context anyway?

            // assert

        }
    }
}
